import pytest

REQUESTS_GET_EXPECTED_RESULT = {
    "title": "Lorem Ipsum",
    "extract": "Lorem ipsum dolor sit amet",
}


def pytest_configure(config):
    config.addinivalue_line("markers", "e2e: mark as end-to-end test.")


def get_wikipedia_url(language):
    return f"{language}.wikipedia.org"


@pytest.fixture
def mock_requests_get(mocker):
    mock = mocker.patch("requests.get")
    mock.return_value.__enter__.return_value.json.return_value = (
        REQUESTS_GET_EXPECTED_RESULT
    )
    return mock
