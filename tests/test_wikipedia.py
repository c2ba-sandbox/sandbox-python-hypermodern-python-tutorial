from c2ba_hypermodern_python import wikipedia
from tests.conftest import get_wikipedia_url


def test_random_page_uses_given_language(mock_requests_get):
    lang = "de"
    wikipedia.random_page(language=lang)
    args, _ = mock_requests_get.call_args
    assert get_wikipedia_url(lang) in args[0]


def test_random_page_uses_en_language_by_default(mock_requests_get):
    wikipedia.random_page()
    args, _ = mock_requests_get.call_args
    assert get_wikipedia_url("en") in args[0]
