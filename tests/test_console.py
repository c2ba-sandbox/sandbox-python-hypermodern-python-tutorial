import pytest
import requests
from typer.testing import CliRunner

from c2ba_hypermodern_python import __version__, console
from tests.conftest import get_wikipedia_url, REQUESTS_GET_EXPECTED_RESULT


@pytest.fixture
def runner() -> CliRunner:
    return CliRunner()


@pytest.fixture
def app_result(runner: CliRunner, mock_requests_get):
    return runner.invoke(console.app)


@pytest.fixture
def mock_wikipedia_random_page(mocker):
    return mocker.patch("c2ba_hypermodern_python.wikipedia.random_page")


def test_app_succeeds(app_result):
    assert app_result.exit_code == 0


def test_app_prints_entry(app_result):
    assert REQUESTS_GET_EXPECTED_RESULT["title"] in app_result.stdout
    assert REQUESTS_GET_EXPECTED_RESULT["extract"] in app_result.stdout


def test_app_invokes_requests_get(app_result, mock_requests_get):
    assert mock_requests_get.called


def test_app_used_expected_url(app_result, mock_requests_get):
    args, _ = mock_requests_get.call_args
    assert get_wikipedia_url("en") in args[0]


def test_app_fails_on_request_error(runner: CliRunner, mock_requests_get):
    mock_requests_get.side_effect = Exception("Boom")
    result = runner.invoke(console.app)
    assert result.exit_code == 1


def test_app_prints_message_on_request_error(runner: CliRunner, mock_requests_get):
    mock_requests_get.side_effect = requests.RequestException("Toto")
    result = runner.invoke(console.app)
    assert "Aborted" in result.output
    assert result.exit_code == 1


def test_app_uses_specified_language(runner: CliRunner, mock_wikipedia_random_page):
    runner.invoke(console.app, ["--language", "pl"])
    mock_wikipedia_random_page.assert_called_with(language="pl")

    runner.invoke(console.app, ["-l", "fr"])
    mock_wikipedia_random_page.assert_called_with(language="fr")

    runner.invoke(console.app, env={"HYPERPYTHON_LANG": "sp"})
    mock_wikipedia_random_page.assert_called_with(language="sp")


def test_app_uses_en_language_by_default(mock_wikipedia_random_page, app_result):
    mock_wikipedia_random_page.assert_called_with(language="en")


def test_version(runner: CliRunner):
    result = runner.invoke(console.app, ["--version"])
    assert result.exit_code == 0
    assert __version__ in result.stdout


@pytest.mark.e2e
def test_app_succeeds_in_production_env(runner: CliRunner):
    result = runner.invoke(console.app)
    assert result.exit_code == 0
