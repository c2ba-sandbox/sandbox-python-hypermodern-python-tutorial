import textwrap

import typer

from . import __version__, wikipedia

app = typer.Typer()


def version_callback(value: bool):
    if value:
        typer.echo(f"hypermodern-python, version {__version__}")
        raise typer.Exit()


@app.command()
def main(
    language: str = typer.Option(
        "en",
        "--language",
        "-l",
        metavar="LANG",
        envvar="HYPERPYTHON_LANG",
        help="Language edition of Wikipedia",
    ),
    version: bool = typer.Option(None, "--version", callback=version_callback),
):
    """The Hypermodern Python project."""
    try:
        data = wikipedia.random_page(language=language)
    except wikipedia.WikipediaException as error:
        typer.echo(str(error), err=True)
        raise typer.Abort()

    title = data["title"]
    extract = data["extract"]

    typer.secho(title, fg="green")
    typer.echo(textwrap.fill(extract))


if __name__ == "__main__":
    app()
