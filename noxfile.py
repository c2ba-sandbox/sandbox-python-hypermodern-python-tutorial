from contextlib import contextmanager
import os
import tempfile

import nox

nox.options.sessions = "lint", "mypy", "safety", "tests"

SOURCE_LOCATIONS = ["src", "tests", "noxfile.py"]


@contextmanager
def temporary_file():
    # On windows we cannot use tempfile.NamedTemporaryFile() directly because
    # the file cannot be written while still open
    tmpf = tempfile.NamedTemporaryFile(delete=False)
    tmpf.close()
    try:
        yield tmpf.name
    finally:
        os.unlink(tmpf.name)


def export_poetry_requirements(session, file_name: str):
    session.run(
        "poetry",
        "export",
        "--dev",
        "--format=requirements.txt",
        "--without-hashes",
        f"--output={file_name}",
        external=True,
    )


def install_with_constraints(session, *args, **kwargs):
    with temporary_file() as requirements:
        export_poetry_requirements(session, requirements)
        session.install(f"--constraint={requirements}", *args, **kwargs)


@nox.session(python=["3.8", "3.7"])
def tests(session):
    args = session.posargs or [
        "--cov-fail-under",
        "100",
        "-m" "not e2e",
    ]
    session.run("poetry", "install", "--no-dev", external=True)
    install_with_constraints(
        session, "coverage[toml]", "pytest", "pytest-cov", "pytest-mock"
    )
    session.run("pytest", *args)


@nox.session(python=["3.8", "3.7"])
def lint(session):
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(
        session,
        "flake8",
        "flake8-bandit",
        "flake8-black",
        "flake8-bugbear",
        "flake8-import-order",
        "pep8-naming",
    )
    session.run("flake8", *args)


@nox.session(python=["3.8", "3.7"])
def safety(session):
    with temporary_file() as requirements:
        export_poetry_requirements(session, requirements)
        session.install("safety")
        session.run("safety", "check", f"--file={requirements}", "--full-report")


@nox.session(python=["3.8", "3.7"])
def mypy(session):
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(session, "mypy")
    session.run("mypy", *args)


@nox.session(python="3.7")
def pytype(session):
    """Run the static type checker pytype."""
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(session, "pytype")
    session.run("pytype", *args)


@nox.session
def black(session):
    args = session.posargs or SOURCE_LOCATIONS
    install_with_constraints(session, "black")
    session.run("black", *args)
