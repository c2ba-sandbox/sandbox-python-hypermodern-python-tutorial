# Tutorial Notes

The Hypermodern Python tutorial walks you through the creation of a complete and up to date Python project structure with:
- unit tests
- static analysis
- type-checking
- documentation
- continuous integration and delivery

Our example project is a simple command-line application, which uses the Wikipedia API to display random facts on the console.

## Chapter 1 - Setup

https://cjolowicz.github.io/posts/hypermodern-python-01-setup/

Topics:
- Setting up a GitHub repository
  - Will do it on Gitlab instead, in a sub-project of my python-sandbox repository
- Installing Python with pyenv
  - Have already pyenv-win installed
- Setting up a Python project using Poetry
  - I've installed poetry using pipx
- Creating a package in src layout
- Managing virtual environments with Poetry
- Managing dependencies with Poetry
- Command-line interfaces with click
  - Will use Typer instead, a framework built on top of click
- Example: Consuming a REST API with requests

### Setting up a Python project using Poetry

Poetry is similar to npm and yarn, but for Python. It is an ideal successor to setuptools. Alternative options are:
- flit
- pipenv
- pyflow
- dephell

To create a new poetry project, we create a directory for the project and run poetry init inside:

```bash
mkdir hypermodern_python
poetry init --no-interaction
```

### Creating a package in src layout

We create the project structure using src layout. The author gives https://hynek.me/articles/testing-packaging/ as a reference.

The author gives the advice to use kebab case for project repository name (`hypermodern-python`) and snake case for package name (`hypermodern_python`).

Here I'm using `c2ba_hypermodern_python` for package name to avoid collision on PyPI.

### Managing virtual environments with Poetry

We now want to create a virtual environment for our project. Poetry can create one and manage it. However by default it will be created outside the project folder. I prefer having it inside, for that we change the global settings of poetry:

```bash
poetry config virtualenvs.in-project true
poetry config --list # To check the setting is now correct
```

Then inside the project we run `poetry install` to create the virtual environment.

### Managing dependencies with Poetry and Command-line interfaces with click

After writing a hello world CLI, we want to add an entry point for it. The entry point will be a script + an executable to run our application. The tutorial tells us to define it with:

```
[tool.poetry.scripts]
hypermodern-python = "hypermodern_python.console:main"
```

However in my case with `typer` the main function takes an argument. So I create another function `entry_point` taking no argument and calling `typer.run(main)`. I define the entry point like this:

```
[tool.poetry.scripts]
hypermodern-python = "c2ba_hypermodern_python.console:entry_point"
```

Also at this point I noticed the `name` entry of my pyproject file was set to `hypermodern_python` which is a mistake since it should be the name of my top level package: `c2ba_hypermodern_python`.

After this I run `poetry install` to install my entry point in the venv. Then I can run my application with one of the following:

```
# with poetry
poetry run hypermodern-python

# with the executable
hypermodern-python

# as python module, thanks to the egg-link file installed
# in the venv site-package by poetry
python -m c2ba_hypermodern_python.console
```

After that the tutorial explains how to fetch the wikipedia API for random facts. We use the textwrap module to print it to the console with text wrapping so that every line is at most 70 chars long by default.

According to PEP8 https://www.python.org/dev/peps/pep-0008/#imports we should group imports like this:
- standard library
- third party packages
- local imports

### Additional Reads

**Testing & Packaging**

https://hynek.me/articles/testing-packaging/

"How to ensure that your tests run code that you think they are running, and how to measure your coverage over multiple tox runs (in parallel!)."

This article discuss the `src` layout, and why it is important.

As mentionned, "isolating the code into a separate - un-importanble - directory might be a good idea". The goal is to avoid having the tests, and whatever else, run against the code directory, but instead like it was installed. Having the extra level of indirection `src` ensure this.

With poetry the package can be installed in the venv, in editable mode, using `poetry install`. With setuptools you need to write a `setup.py` file to do it properly.

Also the author think it is more clear to have directories src, tests and docs at the root of the project instead of many directories whose purpose is unclear.

A drawback of the src layout mentionned by the author are long paths you'll get in test coverage outputs of tox, due to the fact that the package is now installed and run from the virtual env. The workaround is to use a `[paths]` configuration section.

He also explain how to run coverage tests on multiple environment in parallel.

**Dev/Prod Parity**

https://12factor.net/dev-prod-parity

"Keep development, staging, and production as similar as possible"

The article identifies 3 kind of gaps betwwen dev and prod envs:
- The time gap: A developer may work on code that takes days, weeks, or even months to go into production.
- The personnel gap: Developers write code, ops engineers deploy it.
- The tools gap: Developers may be using a stack like Nginx, SQLite, and OS X, while the production deploy uses Apache, MySQL, and Linux.

They advocate the need of reducing these gaps:
- Make the time gap small: a developer may write code and have it deployed hours or even just minutes later.
- Make the personnel gap small: developers who wrote code are closely involved in deploying it and watching its behavior in production.
- Make the tools gap small: keep development and production as similar as possible.

This is basically agile development + CI/CD.

Then mention resisting the urge to use different backing services between development and production, even when adapters theoretically abstract away any differences in backing services. Modern packaging systems combined with light-weight virtual environments (Docker) allow developers to run local environments which closely approximate production environments. **The cost of installing and using these systems is low compared to the benefit of dev/prod parity and continuous deployment.**.

## Chapter 2 - Testing

https://cjolowicz.github.io/posts/hypermodern-python-02-testing/

Topics:

- Unit testing with pytest
- Code coverage with Coverage.py
- Test automation with Nox
- Mocking with pytest-mock
- Example CLI: Refactoring
- Example CLI: Handling exception gracefully
- Example CLI: Selecting the Wikipedia language edition
- Using fakes
- End-to-end testing

### Unit testing with pytest

Pytest website: https://docs.pytest.org/en/latest/

First I'm wondering: What are the differences between unittest and pytest ?

The author mention pytest has become the de factor standard for unit testing in python, but why ? From a redit post https://www.reddit.com/r/Python/comments/5uxh22/unittest_vs_pytest/:

- you can just create test functions, you don't have to create test classes if you don't need that level of organisation… you usually don't
- fixtures are simpler, more explicit, more convenient and more composable than xUnit-style setUp/tearDown[0], they also handle at least one more scoping level
- assertion rewriting provides more error information at less cost than the assert* method, and without the requirement to discover/learn a dozen different methods
- it's way easier to extend pytest, though the documentation is… lacking
- parameterised tests are convenient, they provide much better error output than hand-rolling assertions in a loop
- the test runner is better than unittest's, especially marker-based and name-based test selection e.g. if your tests are properly marked (by their fixture) you can trivially say "run all tests which don't need a database cursor"

To create the unit test for the console application, I need to adapt it to typer instead of click. Fortunately, typer also provides a CliRunner to make this task easy: https://typer.tiangolo.com/tutorial/testing/

To simplify the console app, I replaced my entry_point function with a typer.Typer() instance, which is also used to invoke the CliRunner.

### Code coverage with Coverage.py

Coverage.py website: https://coverage.readthedocs.io/

We install Coverage.py and its pytest extension. We specify the `toml` extra so we can configure it in `pyproject.toml`.

```
poetry add -D coverage[toml] pytest-cov
```

Then running the coverage with `pytest --cov` I get the following result:

```
================================ test session starts ================================
platform win32 -- Python 3.7.5, pytest-6.0.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\projects\code_sandbox\sandbox_python\hypermodern_python
plugins: cov-2.10.0
collected 1 item

tests\test_console.py .                                                        [100%]

----------- coverage: platform win32, python 3.7.5-final-0 -----------
Name                                      Stmts   Miss Branch BrPart  Cover   Missing
-------------------------------------------------------------------------------------
src\c2ba_hypermodern_python\__init__.py       1      0      0      0   100%
src\c2ba_hypermodern_python\console.py       21      3      4      2    80%   14->15, 15-16, 34->35, 35
-------------------------------------------------------------------------------------
TOTAL                                        22      3      4      2    81%


================================= 1 passed in 1.26s =================================
```

It indicates me what lines are not covered by tests. Lets add some tests to cover them.

The first is the `--version` option, quite easy to test. I add a `test_version` that invoke the app with the option. I check if the output contains the version number imported from the package.

We can also generate a coverage xml report with: `pytest --cov --cov-report xml`. This report can be consumed by VSCode extension `Coverage Gutter` to display coverage of each line.

The second uncovered section is the main section of the script. This section cannot be tested with typer CliRunner, lets try with a subprocess but I guess it will not improve the coverage because it is not is the same python process than tests... => Ok I'm surprised, it works !

As a reference, I found this pytest extension also to handle fake subprocesses: https://pypi.org/project/pytest-subprocess/

While it is possible to add an option to make tests fail when coverage is under a certain threshold, it seems it block VSCode from discovering tests.
```
# pyproject.toml
[tool.coverage.report]
fail_under = 100
```

However it is possible to set it on command line: `pytest --cov --cov-fail-under 100`. Can be useful for CI automation.

### Test automation with Nox

Nox website: https://nox.thea.codes/en/stable/index.html

The author recommends nox installation with pip, in user directory. I prefer using pipx:
```
Test automation with Nox
```

We don't install nox as a poetry dependency of the project because it will be used to install the poetry project in an isolated environment. The goal is to be able to run multiple test sessions with different environments.

We specify two sessions: one for python 3.7, one for python 3.8 in the file `noxfile.py`. If nox cannot find one of the python, it simply ignores the session, so we can run it locally.

Running a nox session can be quite expesive because the whole poetry project is reinstalled in an isolated environment (that's the goal !). So locally running tests from the dev env should be prefered, and nox run should be reserved for CI automation.

It is possible to reuse the existing venv with:
```
nox -r
```
To speed up CI, it might be good to have `.nox` folder stored in a cache and to use `-r` option on commits from branches other than master.

After a few local tests, I add a Gitlab CI script to automate running tests. Instead of running nox with all configurations as a single job, I create two jobs and use the `NOXSESSION` env var to run each session separately. I also specify a regexp to display code coverage in Gitlab job result.

So, as far as I understand, nox is useful to:
- specify jobs to automate: testing, linting, etc
- with what Python versions
- defines the steps using a python script (no need for bash scripts)
- ensure a new environment is created for each job

### Mocking with pytest-mock

The goal here is to fake the wikipedia API to avoid depending on it while testing.

For that we add `pytest-mock` as a dev dependency. This package gives us a new fixture `mocker` that we can use to replace some functions calls, such as `requests.get`.

The mechanic is:
- create a new fixture that take `mocker` as argument (kind of fixture composition ?)
- in this fixture, use `mocker.patch(func_name)`
- use `.return_value` of the returned mock object to mock additional functions / attributes of the object that should have been returned by `func_name`
- return the mock object
- use the new fixture where needed

Let's try something, we mock `requests.get`, so lets try to import `get` from `requests` and use it directly, to check if mocking still works.
=> yep, still works !

We can use `.called` on the mock fixture we have written to check if it was called.

We can also use `.call_args` on it to inspect the arguments it was called with.

And we can set `.side_effect` to an exception to trigger it to be raised. Then we can test how our program behave on this exception.

The author recommands a single assert per test case, because more fine-grained test cases make it easier to figure out why the test suite failed when it does.

He also advocates TDD practices: write a test before writing implementatio, it should fail before and success after. This provides confidence that the tests are testing something and do not simply pass because of a flaw in the tests.

### Example CLI: Refactoring

We can now refactor our code in total confidence with all our tests !

This is actually why we write tests. Some tests seems really useless, especially when we test the feature by hand before. But the goal is not to have confirmation your code works right now. The goal is to have confirmation of this in the future, and automatically (you don't want to have to test all your features over and over again when the code change). Human tests remain important, but when a human find a bug, try to create a test for it.

### Example CLI: Handling exceptions gracefully

The author raise a ClickException from the `random_page` method is case of a RequestException. Instead I choose to create a new WikipediaException type and to raise it, because I think it is not the responsability of random_page to know it is called from a click application. At the call site I catch it, print it as error, and raise a `typer.Abort`.

After more testing, offline, I noticed my main section test cannot pass when offline because its call to requests.get is not mocked. However, I don't really know how to mock a call in a subprocess. So lets try `pytest-subprocess`.

=> While it seems to work at first, it does not. It seems it just don't call the real subprocess, which is not what I want ! Let uninstall it.

I choose an alternative strategy: stop testing the main section, since it only calls `main()` and main is already testing, and exclude it from the coverage report to still reach 100% coverage.

To do that I add the following line in pyproject.toml:
```toml
exclude_lines = ["if __name__ == .__main__.:"]
```

### Example CLI: Selecting the Wikipedia language edition

To factorize our fixtures between multiple test modules, we put them in `conftest.py`. Pytest automatically import fixtures from this module.

Note: in console we must use `python -m pytest` in root folder to be able to include modules from `tests` directory.

We moch the `wikipedia.random_page` call and use the assert_called_with method on the mock to check that the language is passed.

After writing our failing tests, we implement the functionality both on the function and on the app using `typer.Option`.

### Using fakes

This section discuss the advantage of using fakes instead of mock for testing. Mock can be too tightly coupled to implementation (mocking `requests.get` assumes we are using requests for http requests, what if we want to use another package ?).

Large data objects can be generated by test object factories, instead of being replaced by mock objects (check out the excellent [factoryboy](https://factoryboy.readthedocs.io/) package).

If is mainly just a discussion because implementing a fake http API for testing is out of scope here. However it shoes how to `tearDown` data of a fixture, like we can do in general. For a fake API this would be required because we need to start a thread and a socket connection, so we need to destroy it at the end of the test.

Since pytest fixtures are just objects returned from function, we cannot have a tearDown method like most unit test frameworks. Instead we use a generator: we `yield` the result instead of returning it, and we put our tear down code after the yield.

Example:
```python
@pytest.fixture
def fake_api():
    api = FakeAPI.create()
    yield api
    api.shutdown()
```

By default, fixtures are created once per test function. Instead, you could create the fake API server once per test session, which is useful for expensive features creation/deletion:

```python
@pytest.fixture(scope="session")
def fake_api():
    api = FakeAPI.create()
    yield api
    api.shutdown()
```

### End-to-end testing

Using pytest **markers**, we can include/exclude some tests from a specific run. We can mark some tests `e2e` for end-to-end to implement some tests that use the production environment: use the real wikipedia. Since such tests are slow and unrelyable, we do not run them automatically. But it can be useful to run them locally while developing, since it bring confidence.

For that we define the `pytest_configure` function in conftest.py, in which we declare our marker `e2e`. In the noxfile we disable it by default, so tests maked e2e will not be run by CI. By default it is run from VSCode, or in console.

## Chapter 3: Linting

https://cjolowicz.github.io/posts/hypermodern-python-03-linting/

Topics:
- Linting with Flake8
- Code formatting with Black
- Checking imports with flake8-import-order
- Finding more bugs with flake8-bugbear
- Identifying security issues with Bandit
- Finding security vulnerabilities in dependencies with Safety
- Managing dependencies in Nox sessions with Poetry
- Managing Git hooks with pre-commit

### Linting with Flake8

For linting I install flake8 as a poetry dependency and configure VScode settings.json to have it enabled. I follow the tutorial to install a nox job to run it, and modify my CI script to run it on Gitlab CI. I put tests and lint in the same stage to have them run concurrently.

There is an awesome list for flake8 extensions: https://github.com/DmytroLitvinov/awesome-flake8-extensions. This list as been built after the following blog post: https://julien.danjou.info/the-best-flake8-extensions/

Let's follow the tutorial before installing some of them after.

### Code formatting with Black

For black we add a section to the noxfile that can only be run manually. We install the `flake8-black` plugin to be warned about black formatting issues, and change flake8 config according to black.

I also change my VScode settings.json to make it explicit that black is used for this project, and that formatting should be run after each file save.

### Checking imports with flake8-import-order

We install `flake8-import-order` as a poetry dev dependency, and in the noxfile. We have to specify what packages should be considered local: c2ba_hypermodern_python and tests. The author recommands selecting the google import order style. Since I don't know any other, and I prefer to be explicit about the followed convention, I select it also.

The author mention that import linters are currently a very active area, and that `flake8-import-order` has been put in maintenance mode, in favor of `flake8-isort` (https://github.com/PyCQA/flake8-import-order/issues/163#issuecomment-468923340; https://timothycrosley.github.io/isort/).

If would probably be worth to investigate a bit further before creating my cookiecutter.

### Finding more bugs with flake8-bugbear

flake8-bugbear webpage: https://github.com/PyCQA/flake8-bugbear

An extension I already use in other projects that perform static code analysis to search for potential bugs.

Now an issue that is raised by bugbear is B008 when defining typer Option and Argument on function prototypes:
> B008 Do not perform function calls in argument defaults.  The call is performed only once at function definition time. All calls to your function will reuse the result of that definition-time function call. If this is intended, assign the function call to a module-level variable and use that variable as a default value.

So we need to ignore it on faulty lines. ~~Unfortunately is does not seem we can ignore it on a whole file. I don't want to disable it at the project level because it seems useful when this is indeed not made intentionnaly.~~ We can do this with:
```
# .flake8
[flake8]
per-file-ignores = src/c2ba_hypermodern_python/console.py:B008
...
```

### Identifying security issues with Bandit

Bandit webpage: https://github.com/PyCQA/bandit

Bandit finds known issues that can be detected via static file checking. If you are very concerned with security, you should consider using additional tools, for example a fuzzing tool such as [python-afl](https://github.com/jwilk/python-afl).

Bandit uses `assert` to enforce interface constraints because they are removed when compiling to optimized byte code. Consequently, we must remove S101 from test files:
```
# .flake8
[flake8]
per-file-ignores = tests/*:S101
...
```
This show me how to ignore some warnings for a specific file, so I can now add this for B008 on file console.py !

### Finding security vulnerabilities in dependencies with Safety

Safety webpage: https://github.com/pyupio/safety

Safety checks the dependencies of your project for known security vulnerabilities, using a curated database of insecure Python packages.

It is probably similar to Github security issues reporting and auto merging.

Here we use the `export` command of poetry to create a `requirements.txt` file, to be passed to safety. We do this in the noxfile.

### Managing dependencies in Nox sessions with Poetry

This part is great as it explains how to run install in nox sessions but contraints with requirements generated by poetry.

That way we can reproduce the dev env dependency versions from the lock file and ensure the CI run with the same versions. This is important to create deterministic linter checks.

This could be used later if I want to test the package by installing a generated wheel instead of the source code directly.

We also make a more lightweight installation for tests nox job, by not installing all dev dependencies of the package but only test packages, with constraints.

### Managing Git hooks with pre-commit

pre-commit webpage: https://pre-commit.com/

git hooks webpage: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks

This last section is awesome as it demonstrates how to easily include git hooks using `pre-commit` package, both online standard hooks and locally defined hooks (black, flake8, ...) to avoid version mismatch.

I choose to install `pre-commit` as a dev dependency with poetry, to make it explicit that this project uses git hooks (even if the file `.pre-commit-config.yaml` should be enough, it ensures a developper installing this project with poetry also have pre-commit in the same version).

A warning: git hooks are installed at the repo level, and since this project is right now in a subdirectory, hooks will be applied to all other directories.

Also the hook installed by pre-commit try to run /usr/bin/env python.exe, which does not work by default when python is installed with pyenv-win (the shim installed by pyenv-win is named `python`, not `python.exe`). Some ways to solve this:
- Always commit with the venv activated. in that case vscode cannot be used to commit...
- Fix the hook, but when running `pre-commit install` again you will have to fix it again...
- Copy the shim `python` into `python.exe`, in the same directory of pyenv-win shims; must be redo when installing local shims...
- Add an alias python.exe='python' in my bashrc => does not work because of "/usr/bin/env"

For now I choose to change the commit hook manually. Also it seems when I change the pre-commit config, I don't need to re-run the install step.

At this point I hit a problem: poetry cannot be run from the root repository because the project is in a subfolder... I think it is time to extract the code in a dedicated repository !

=> OK, project extracted here https://gitlab.com/c2ba_sandbox/sandbox-python-hypermodern-python-tutorial with the full code history + exported issues

I add `pep8-naming` package for linting of naming conventions.

With the changes of the noxfile, the fact that we install poetry project without dev dependencies, it start to become more expensive to run `nox -rs tests` -> poetry start by uninstalling all previous dev dependencies from the nox venv. I don't want to put back the install of dev dependencies because it speeds up Gitlab CI run.

So an alternative would be to run tests locally with the `pytest` command, but many command line options are defined in the nox file. To add them by default, we can define them in `pyproject.toml`, section `[tool.pytest.ini_options]`, entry `addopts`. So I split options that I always want, in `pyproject.toml`, and options for Gitlab CI, only in the noxfile. Nox can still be run locally, but it is more expensive that just running `pytest`.

As a side note, I don't put the `--cov-fail-under 100` in `pyproject.toml` because it seems to confuse VScode test discovery.

## Chapter 4 - Typing

https://cjolowicz.github.io/posts/hypermodern-python-04-typing/

Topics:
- Type annotations and type checkers
- Static type checking with mypy
- Static type checking with pytype
- Adding type annotations to the package
- Data validation usiing Desert and Marshmallow
- Runtime type checking with Typeguard
- Increasing type coverage with flake8-annotations
- Adding type annotations to Nox sessions
- Adding type annocations to the test suite

### Type annotations and type checkers

Type annotations module: https://docs.python.org/3/library/typing.html

- Introduced in Python 3.5
- Not checked at runtime
- Static type checkers use type annotations to check type correctness of a program and find potential bugs
- Pioneer of type checkers is [mypy](http://mypy-lang.org/)
- Big companies announced their own static type checkers:
  - Google [pytype](https://google.github.io/pytype/)
  - Facebook [pyre](https://pyre-check.org/)
  - Microsoft [pyright](https://github.com/microsoft/pyright)
- PyCharm also ships with a static type checker

The tutorial covers mypy and pytype, as well as Typeguard, a runtime type checker.

### Static type checking with mypy

mypy webpage: http://mypy-lang.org/

- Mypy is added to the project as a dev dependency.
- We define a nox session to run it, and put it in default sessions
- We disable explicitly type checking for some dependencies (nox, pytest, typer). As many projects are actively working on typing support, the list can be cut down.

### Static type checking with pytype

pytype webpage: https://google.github.io/pytype/

- Only available for Python 3.7 right now (https://github.com/google/pytype/issues/440)
  - This gives us the opportunity to see how to add a poetry dependency only for a specific python version

After a few trial, it seems pytype does not work on windows (I get errors about windows path separators...) I give up on it since I'm mainly interested about mypy.

## Questions that remains

- Tests are run in CI on the source repository directly (`poetry install` installs in edit mode the package). Should'nt we build a wheel, install the wheel (with nox install), then run the tests instead ? (warning: we then need to install pytest manually, as well as other dev dependencies with nox; note that we can use poetry to create the `requirements.txt` file that match the dev environment).
- Should I choose `flake8-isort` over `flake8-import-order`, since the second one is in maintenance mode ?
- Some tools are installed globally instead of locally with poetry (pre-commit), why ?
- How to fix pre-commit using "/usr/bin/env python.exe" ?
- Why `--cov-fail-under 100` confuses VSCode test discovery ?
- Is pytype working on windows ?
