# c2ba Hypermodern Python Project

Follow this tutorial https://cjolowicz.github.io/posts/hypermodern-python-01-setup/

More details about my process here: docs/tutorial_notes.md

## Developer Setup

You need to have `poetry` installed. You can create a virtual env in `.venv` folder, activate it and install it inside, or install it globally on your system (I recommand using `pipx`).

Then run the following command at the root of the repository:
```bash
poetry install
```

If should create a virtual env in `.venv` and install dependencies for the project.

After that you caan activate the virtual env and run the test suite and linting with nox:
```bash
source .venv/Scripts/activate # or source .venv/bin/activate on linux, or have nox installed globally
nox -r
```

For quicker testing, you can run `pytest` in the console with the venv activated. However it will not be executed in a dedicated virtual env, but in the developer virtual env `.venv`. You can also run the tests from VSCode.

You can also install the pre-commit hook:
```bash
# With the virtual env activated, or pre-commit installed globally
pre-commit install
```

If you have python installed with pyenv-win, you may need to manually update `.git/hooks/pre-commit` after to replace `python.exe` with `python` on the first line.
